<h1 align="center">
  <br>
  Object detection-Ciafar-10
  <br>
</h1>

## Machine learning algorithm implemintation 

Program demonstates the standart way of classification objects using the CIAFAR-10 dataset

## Installing
Using one of the follow: 
* Jyputer-Notebook
* Google Collab

## Methods 
*  Support Vector Machine (SVM) 
* Convolutional Neural Network 




## Results 
### CNN

![Alt Text](CNN.png)
![Alt Text](Matrix.png)
* For training CNN  we have used 3 dense layers with softmax at the end. This model used 1500 epoch and took about 30 minutes to train on the batch size 50 on the GPU. As it showed on the figure it trains relatively normal with this learning rate and epoch size hoverer we should be careful of overfitting on the last 1500 epochs. In terms of results, CNN performed with an average accuracy of 38 per cent. This is due to the complexity of the algorithm. After when training finished we can plot the confusion matrix which shows the average accuracy on each CIAFAR-10 dataset.



### SVM:
![Alt Text](SVM.png)
![Alt Text](Matrix2.png)

* SVM compare to CNN has better accuracy which is 56 per cent on the test dataset. For this algorithm, we have used the Radial Basis function kernel and C as the penalty for misclassifying a data point equal to 3 which helps to us achieve the medium variance and medium bias. Nevertheless, we have used Support vector classification which allows us to predict multi-class data from our dataset. According to the confusion matrix, we can see that SVM predicts more classes than CNN with 65 per cent on each class respectively.


## Conclusion 
In our point of view, both algorithms are very sufficient to perform to object detection. However, there is always room for improvement. For instance, in CNN we could change the size of the test dataset to make sure that our data not overfits and having more validation data for training. Furthermore, the addition of epochs and number of dense layers could help to increase the accuracy but the learning rate will be slower. SVM is a clear winner but SVM could be better in different ways such as changing the Principal Analysis Components or value of the misclassifying data point.




## Built With


* Python 3.6
* Tensorflow
* Keras


## Authors
Arthur Zinnurov(https://gitlab.com/ZinnurovArtur)

## License

MIT
